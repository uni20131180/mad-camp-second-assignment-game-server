// server.js
var express = require('express');
var app = express();
var http = require('http').Server(app); //1
var io = require('socket.io')(http);    //1

var count=1;

var nowConnectUser = new Array();
var room = new Array( {"UserArray" : new Array()}, {"UserArray" : new Array()}, {"UserArray" : new Array()}, {"UserArray" : new Array()} );
var gameState = new Array();
var executeGame = new Array(false,false,false,false);

function game(roomN,userN){
  this.roomNumber = roomN;
  this.userNumber = userN
  this.bulletCount = 0;
  this.character = new Array(
    {"die" : 0, "exist" : 0, "id" : 0, "x_position" : 0, "y_position" : 0, "x_speed" : 0, "y_speed" : 0, "x_destination" : 0, "y_destination" : 0, "hp" : 10000, "hp_full" : 5000, "attack_cooldown" : 0, "attack_usage" : 0, "skill_cooldown" : 0, "skill_max_cooldown" : 0, "skill_usage" : 0, "class" : "warrior", "aggrosive" : 0, "mujeok_time" : 0},
    {"die" : 0, "exist" : 0, "id" : 0, "x_position" : 0, "y_position" : 0, "x_speed" : 0, "y_speed" : 0, "x_destination" : 0, "y_destination" : 0, "hp" : 10000, "hp_full" : 5000, "attack_cooldown" : 0, "attack_usage" : 0, "skill_cooldown" : 0, "skill_max_cooldown" : 0, "skill_usage" : 0, "class" : "healer" , "aggrosive" : 0, "mujeok_time" : 0},
    {"die" : 0, "exist" : 0, "id" : 0, "x_position" : 0, "y_position" : 0, "x_speed" : 0, "y_speed" : 0, "x_destination" : 0, "y_destination" : 0, "hp" : 10000, "hp_full" : 5000, "attack_cooldown" : 0, "attack_usage" : 0, "skill_cooldown" : 0, "skill_max_cooldown" : 0, "skill_usage" : 0, "class" : "dealer" , "aggrosive" : 0, "mujeok_time" : 0},
    {"die" : 0, "exist" : 0, "id" : 0, "x_position" : 0, "y_position" : 0, "x_speed" : 0, "y_speed" : 0, "x_destination" : 0, "y_destination" : 0, "hp" : 10000, "hp_full" : 5000, "attack_cooldown" : 0, "attack_usage" : 0, "skill_cooldown" : 0, "skill_max_cooldown" : 0, "skill_usage" : 0, "class" : "dealer" , "aggrosive" : 0, "mujeok_time" : 0},
  );
  this.boss = {
    "x_position" : 0, "y_position" : 0, "x_speed" : 0, "y_speed" : 0,
  "hp" : 100000, "hp_full" : 100000, "now_aggrouser" : -1,
   "attack_kind" : "0" , "attack_usage" : "0" ,"attack_cooldown" : 0, "attack_ready" : 0, "attack_max_charge" : 0,
   "attack_x" : 0, "attack_y" : 0, "attack_radius" : 0,
   "pattern_kind" : "0", "pattern_usage" : 0, "pattern_maintaincount" : 0, "pattern_cooldown" : 0, "pattern_miss" : 0 ,
   "pattern_x1" : 0, "pattern_y1" : 0, "pattern_x2" : 0, "pattern_y2" : 0, "pattern_x3" : 0, "pattern_y3" : 0, "pattern_x4" : 0, "pattern_y4" : 0,
   "pattern_radius1" : 0, "pattern_radius2" : 0, "pattern_radius3" : 0, "pattern_radius4" : 0,
   "pattern_time1" : 0, "pattern_time2" : 0, "pattern_time3" : 0, "pattern_time4" : 0,
   "pattern_max_time1" : 0, "pattern_max_time2" : 0, "pattern_max_time3" : 0, "pattern_max_time4" : 0,
   "pattern_success1" : 0, "pattern_success2" : 0, "pattern_success3" : 0, "pattern_success4" : 0,
   "dontmove" : 0, "boss_rage" : 0, "boss70_pattern" : 0, "boss30_pattern" : 0,
  };
  this.bossMissile = new Array();

  this.p_attack = new Array(0,0,0,0);
  this.p_hit = new Array(0,0,0,0);
  this.p_skill = new Array(0,0,0,0);
  this.p_skill_hit = new Array(0,0,0,0);
  this.bossPatternAttackFactory = function(){
    // 패턴은 두개중에 하나를 쓸 것이다.
    // 현재 특수패턴 시전중이 아닐 때!, 70% 30% 낮아졌을 때 패턴을 시작한다.
    if( (this.boss["pattern_usage"] == 0 ) && ( ((this.boss["hp"] < 0.7 * this.boss["hp_full"]) && (this.boss["boss70_pattern"] ==0 )) || ((this.boss["hp"] < 0.3 * this.boss["hp_full"]) && (this.boss["boss30_pattern"]==0 )) ) )  {
        io.to('Room'+this.roomNumber).emit("special");
        if(this.boss["hp"] < 0.3 * this.boss["hp_full"]){
            this.boss["boss30_pattern"] = 1;
        }else{
            this.boss["boss70_pattern"] = 1;
        }
        this.boss["pattern_usage"] = 1;

        var a = String.fromCharCode( Math.floor(Math.random() * 2) + 97);
        this.boss["pattern_kind"] = a;
        switch(a){
          case 'a':
            // 힐 페이즈 1회 지속
            // 석세스는 1은 성공이다.
            // 제한시간내에 네개 전부 1로 만들지 못하면 보스는 체력을 회복한다.
            // 패턴 종료 후 pattern_usage 를 0으로 만든다.
            this.boss["pattern_maintaincount"] = 4;
            this.boss["pattern_max_time1"] = 800/(this.userNumber);
            this.boss["pattern_max_time2"] = 800/(this.userNumber);
            this.boss["pattern_max_time3"] = 800/(this.userNumber);
            this.boss["pattern_max_time4"] = 800/(this.userNumber);
            this.boss["pattern_time1"] = 0;
            this.boss["pattern_time2"] = 0;
            this.boss["pattern_time3"] = 0;
            this.boss["pattern_time4"] = 0;
            this.boss["pattern_x1"] = 50;
            this.boss["pattern_y1"] = 50;
            this.boss["pattern_radius1"] = 40;
            this.boss["pattern_x2"] = 210;
            this.boss["pattern_y2"] = 50;
            this.boss["pattern_radius2"] = 40;
            this.boss["pattern_x3"] = 50;
            this.boss["pattern_y3"] = 360;
            this.boss["pattern_radius3"] = 40;
            this.boss["pattern_x4"] = 210;
            this.boss["pattern_y4"] = 360;
            this.boss["pattern_radius4"] = 40;
            this.boss["pattern_success1"] = 0;
            this.boss["pattern_success2"] = 0;
            this.boss["pattern_success3"] = 0;
            this.boss["pattern_success4"] = 0;
          break;
          case 'b':
            // 폭피 패턴 1 회 지속
            // 폭피 패턴은 장전중에는 석세스는 0, 발사 뒤에는 석세스는 1이 된다.
            this.boss["pattern_maintaincount"] = 4;
            this.boss["pattern_max_time1"] = 120;
            this.boss["pattern_max_time2"] = 240;
            this.boss["pattern_max_time3"] = 360;
            this.boss["pattern_max_time4"] = 480;
            this.boss["pattern_time1"] = 0;
            this.boss["pattern_time2"] = 0;
            this.boss["pattern_time3"] = 0;
            this.boss["pattern_time4"] = 0;
            this.boss["pattern_x1"] = 70;
            this.boss["pattern_y1"] = 130;
            this.boss["pattern_radius1"] = 70;
            this.boss["pattern_x2"] = 170;
            this.boss["pattern_y2"] = 130;
            this.boss["pattern_radius2"] = 70;
            this.boss["pattern_x3"] = 70;
            this.boss["pattern_y3"] = 340;
            this.boss["pattern_radius3"] = 70;
            this.boss["pattern_x4"] = 170;
            this.boss["pattern_y4"] = 340;
            this.boss["pattern_radius4"] = 70;
            this.boss["pattern_success1"] = 0;
            this.boss["pattern_success2"] = 0;
            this.boss["pattern_success3"] = 0;
            this.boss["pattern_success4"] = 0;
          break;
        }
    }
  }

  this.bossPatternAttackExecute = function(){
    // 제한시간내에 네개 전부 1로 만들지 못하면 보스는 체력을 회복한다.
    // 패턴 종료 후 pattern_usage 를 0으로 만든다.
    if(this.boss["pattern_usage"]==1){
        this.boss["pattern_time1"]+=1;
        this.boss["pattern_time2"]+=1;
        this.boss["pattern_time3"]+=1;
        this.boss["pattern_time4"]+=1;
        // 시간을 1 프레임씩 옮긴다.
        if(this.boss["pattern_time1"] >= this.boss["pattern_max_time1"]){
          this.boss["pattern_time1"] = this.boss["pattern_max_time1"];
        }
        if(this.boss["pattern_time2"] >= this.boss["pattern_max_time2"]){
          this.boss["pattern_time2"] = this.boss["pattern_max_time2"];
        }
        if(this.boss["pattern_time3"] >= this.boss["pattern_max_time3"]){
          this.boss["pattern_time3"] = this.boss["pattern_max_time3"];
        }
        if(this.boss["pattern_time4"] >= this.boss["pattern_max_time4"]){
          this.boss["pattern_time4"] = this.boss["pattern_max_time4"];
        }

        // 힐패턴일때랑 폭피패턴 따로 짜야하니까 케이스를 나누자.
        switch(this.boss["pattern_kind"]){
          case 'a':
            // 힐 패턴
            // 캐릭터가 원 위에 올라가면 원이 꺼져야 한다.
            for(var i = 0; i<this.userNumber;i++){
              var distance_x = (this.character[i]["x_position"] - this.boss["pattern_x1"]) * (this.character[i]["x_position"] - this.boss["pattern_x1"]);
              var distance_y = (this.character[i]["y_position"] - this.boss["pattern_y1"]) * (this.character[i]["y_position"] - this.boss["pattern_y1"]);
              var distance = distance_x + distance_y;
              if(  distance < this.boss["pattern_radius1"] * this.boss["pattern_radius1"] ){
                this.boss["pattern_success1"] = 1;
              }

              distance_x = (this.character[i]["x_position"] - this.boss["pattern_x2"]) * (this.character[i]["x_position"] - this.boss["pattern_x2"]);
              distance_y = (this.character[i]["y_position"] - this.boss["pattern_y2"]) * (this.character[i]["y_position"] - this.boss["pattern_y2"]);
              distance = distance_x + distance_y;
              if(  distance < this.boss["pattern_radius2"] * this.boss["pattern_radius2"] ){
                this.boss["pattern_success2"] = 1;
              }

              distance_x = (this.character[i]["x_position"] - this.boss["pattern_x3"]) * (this.character[i]["x_position"] - this.boss["pattern_x3"]);
              distance_y = (this.character[i]["y_position"] - this.boss["pattern_y3"]) * (this.character[i]["y_position"] - this.boss["pattern_y3"]);
              distance = distance_x + distance_y;
              if(  distance < this.boss["pattern_radius3"] * this.boss["pattern_radius3"] ){
                this.boss["pattern_success3"] = 1;
              }

              distance_x = (this.character[i]["x_position"] - this.boss["pattern_x4"]) * (this.character[i]["x_position"] - this.boss["pattern_x4"]);
              distance_y = (this.character[i]["y_position"] - this.boss["pattern_y4"]) * (this.character[i]["y_position"] - this.boss["pattern_y4"]);
              distance = distance_x + distance_y;
              if(  distance < this.boss["pattern_radius4"] * this.boss["pattern_radius4"] ){
                this.boss["pattern_success4"] = 1;
              }
            }

            if(this.boss["pattern_max_time1"] <= this.boss["pattern_time1"]){
              if(this.boss["pattern_success1"] == 0){
                  this.boss["hp"]+=this.boss["hp_full"] * 0.05;
                  this.boss["pattern_maintaincount"]-=1;
              }
            }
            if(this.boss["pattern_max_time2"] <= this.boss["pattern_time2"]){
              if(this.boss["pattern_success2"] == 0){
                  this.boss["hp"]+=this.boss["hp_full"] * 0.05;
                  this.boss["pattern_maintaincount"]-=1;
              }
            }
            if(this.boss["pattern_max_time3"] <= this.boss["pattern_time3"]){
              if(this.boss["pattern_success3"] == 0){
                  this.boss["hp"]+=this.boss["hp_full"] * 0.05;
                  this.boss["pattern_maintaincount"]-=1;
              }
            }
            if(this.boss["pattern_max_time4"] <= this.boss["pattern_time4"]){
              if(this.boss["pattern_success4"] == 0){
                  this.boss["hp"]+=this.boss["hp_full"] * 0.05;
                  this.boss["pattern_maintaincount"]-=1;
              }
            }

            if(this.boss["pattern_maintaincount"]  <= 0){
              this.boss["pattern_usage"] = 0;
            }


          break;

          case 'b':
            // 폭피 패턴

            if(this.boss["pattern_max_time1"] <= this.boss["pattern_time1"]){
              if(this.boss["pattern_success1"] == 0){
                  this.boss["pattern_maintaincount"]-=1;
                  for(var i = 0; i<this.userNumber;i++){
                    var distance_x = (this.character[i]["x_position"] - this.boss["pattern_x1"]) * (this.character[i]["x_position"] - this.boss["pattern_x1"]);
                    var distance_y = (this.character[i]["y_position"] - this.boss["pattern_y1"]) * (this.character[i]["y_position"] - this.boss["pattern_y1"]);
                    var distance = distance_x + distance_y;
                    if(  distance < this.boss["pattern_radius1"] * this.boss["pattern_radius1"] ){
                        this.character[i]["hp"] /= 2;
                    }
                  }
                  this.boss["pattern_success1"] = 1;
              }
            }
            if(this.boss["pattern_max_time2"] <= this.boss["pattern_time2"]){
              if(this.boss["pattern_success2"] == 0){
                  this.boss["pattern_maintaincount"]-=1;
                  for(var i = 0; i<this.userNumber;i++){
                    var distance_x = (this.character[i]["x_position"] - this.boss["pattern_x2"]) * (this.character[i]["x_position"] - this.boss["pattern_x2"]);
                    var distance_y = (this.character[i]["y_position"] - this.boss["pattern_y2"]) * (this.character[i]["y_position"] - this.boss["pattern_y2"]);
                    var distance = distance_x + distance_y;
                    if(  distance < this.boss["pattern_radius2"] * this.boss["pattern_radius2"] ){
                        this.character[i]["hp"] /= 2;
                    }
                  }
                  this.boss["pattern_success2"] = 1;
              }
            }
            if(this.boss["pattern_max_time3"] <= this.boss["pattern_time3"]){
              if(this.boss["pattern_success3"] == 0){
                  this.boss["pattern_maintaincount"]-=1;
                  for(var i = 0; i<this.userNumber;i++){
                    var distance_x = (this.character[i]["x_position"] - this.boss["pattern_x3"]) * (this.character[i]["x_position"] - this.boss["pattern_x3"]);
                    var distance_y = (this.character[i]["y_position"] - this.boss["pattern_y3"]) * (this.character[i]["y_position"] - this.boss["pattern_y3"]);
                    var distance = distance_x + distance_y;
                    if(  distance < this.boss["pattern_radius3"] * this.boss["pattern_radius3"] ){
                        this.character[i]["hp"] /= 2;
                    }
                  }
                  this.boss["pattern_success3"] = 1;
              }
            }
            if(this.boss["pattern_max_time4"] <= this.boss["pattern_time4"]){
              if(this.boss["pattern_success4"] == 0){
                  this.boss["pattern_maintaincount"]-=1;
                  for(var i = 0; i<this.userNumber;i++){
                    var distance_x = (this.character[i]["x_position"] - this.boss["pattern_x4"]) * (this.character[i]["x_position"] - this.boss["pattern_x4"]);
                    var distance_y = (this.character[i]["y_position"] - this.boss["pattern_y4"]) * (this.character[i]["y_position"] - this.boss["pattern_y4"]);
                    var distance = distance_x + distance_y;
                    if(  distance < this.boss["pattern_radius4"] * this.boss["pattern_radius4"] ){
                        this.character[i]["hp"] /= 2;
                    }
                  }
                  this.boss["pattern_success4"] = 1;
              }
            }

            if(this.boss["pattern_maintaincount"]  <= 0){
              this.boss["pattern_usage"] = 0;
            }

          break;
        }

    }

  }

  this.bossNormalAttackFactory = function(){
      // 보스의 노말 패턴을 생성한다.
      var aggro = this.boss["now_aggrouser"];
      if(aggro < 0 || aggro > 3){
        aggro = 0;
      }
      var distance_x = (this.character[aggro]["x_position"] - this.boss["x_position"]) * (this.character[aggro]["x_position"] - this.boss["x_position"]);
      var distance_y = (this.character[aggro]["y_position"] - this.boss["y_position"]) * (this.character[aggro]["y_position"] - this.boss["y_position"]);
      var d_byaggro = distance_x + distance_y;
      if(( this.boss["attack_usage"] == 0 )&&( this.boss["attack_cooldown"] <= 0 ) && (d_byaggro < 5000) ){
          // attack_ready 는 공격이 얼마나 모였느냐를 보는 것이고
          // attack_max_charge 는 얼마나 모아야하는지를 나타내는 변수이다.
          // 공격이 한번 생성되고 나면 보스는 그동안 움직이지 않는다.
          // 공격이 적중 하고 나면 보스는 다시 움직인다.
          // 생성하는 것 자체는 attack_kind 를 설정하고 나서 attack_usage 를 1로 바꾸는 것
          // 그것에 맞추어서 보스 패턴의 공격 범위를 지정하는 일이다.
          var a = String.fromCharCode( Math.floor(Math.random() * 3) + 65);
          this.boss["attack_usage"] = 1;
          this.boss["attack_kind"] = a;
          this.boss["dontmove"] = 1;
          switch(a){
            case 'A' : // 어글자 방향으로 찍기
              this.boss["attack_cooldown"] = 63*3;
              this.boss["attack_ready"] = 0;
              this.boss["attack_max_charge"] = 63;
              this.boss["attack_x"] = this.character[aggro]["x_position"];
              this.boss["attack_y"] = this.character[aggro]["y_position"];
              this.boss["attack_radius"] = 40;
              break;
            case 'B' : // 어글자 반대 방향으로 찍기
              this.boss["attack_cooldown"] = 63*3;
              this.boss["attack_ready"] = 0;
              this.boss["attack_max_charge"] = 63;
              this.boss["attack_x"] = 2*this.boss["x_position"] - this.character[aggro]["x_position"];
              this.boss["attack_y"] = 2*this.boss["y_position"] - this.character[aggro]["y_position"];
              this.boss["attack_radius"] = 50;
              break;
            case 'C' : // 주변 찍기 (거의 피하기 힘들다)
              this.boss["attack_cooldown"] = 63;
              this.boss["attack_ready"] = 0;
              this.boss["attack_max_charge"] = 31;
              this.boss["attack_x"] = this.boss["x_position"];
              this.boss["attack_y"] = this.boss["y_position"];
              this.boss["attack_radius"] = 40;
              break;
          }
          this.bulletCount -= 1;
          if(this.bulletCount < 0){
            this.bulletCount = 0;
          }
      }else if((d_byaggro > 5000)){
        if(this.bulletCount <= 0){
          this.boss["attack_ready"] = 0;
          this.boss["attack_max_charge"] = 63*2
          var k = (Math.floor(Math.random() * 5) + 13);
          for(var i = 0; i < k; i++){
            var x_distance = Math.floor(Math.random() * 2);
            var y_distance = Math.floor(Math.random() * 2);
            if(x_distance == 0){
              x_distance = -1;
            }else{
              x_distance = 1;
            }
            if(y_distance == 0){
              y_distance = -1;
            }else{
              y_distance = 1;
            }
            var x_speed = x_distance * (Math.random() + 0.3) * 1.5;
            var y_speed = y_distance * (Math.random() + 0.3) * 1.5;
            this.bossMissile[i] = {"radius" : 20, "radius_s" : 5, "x_position" : this.boss["x_position"], "y_position" : this.boss["y_position"], "x_speed" : x_speed  , "y_speed" : y_speed  };
            this.boss["attack_usage"] = 0;
            this.bulletCount = 100;
          }
        }else{
          this.bulletCount -= 1;
          if(this.bulletCount < 0){
            this.bulletCount = 0;
          }
        }
      }
  }

  this.init = function(){

    for(var i = 0; i<this.character.length;i++){
      this.character[i]["exist"] = 0;
      this.character[i]["id"] = 0;
      this.character[i]["x_position"] = 0;
      this.character[i]["y_position"] = 0;
      this.character[i]["x_speed"] = 0;
      this.character[i]["y_speed"] = 0;
      this.character[i]["x_destination"] = 0;
      this.character[i]["y_destination"] = 0;
      this.character[i]["hp"] = 10000;
      this.character[i]["attack_cooldown"] = 0;
      this.character[i]["skill_cooldown"] = 0;
      this.character[i]["attack_usage"] = 0;
      this.character[i]["skill_usage"] = 0;
      this.character[i]["class"] = "warrior";
      this.character[i]["aggrosive"] = 0;
      this.character[i]["die"] = 0;
    }
    this.boss["x_position"] = 120;
    this.boss["y_position"] = 240;
    this.boss["x_speed"] = 0;
    this.boss["y_speed"] = 0;
    this.boss["hp"] = 100000;
    this.boss["attack"] = "0"
    this.boss["attack_cooldown"] = 0;
    this.boss["attack_usage"] = "0"
    this.boss["pattern"] = "0"
    this.boss["pattern_maintaintime"] = 0;
    this.boss["pattern_cooldown"] = 0;
    this.boss["pattern_usage"] = 0;
    this.boss["dontmove"] = 0;
    this.boss["rage"] = 0;
    this.boss["pattern_miss"] = 0;
    this.boss["boss70_pattern"] = 0;
    this.boss["boss30_pattern"] = 0;
  }

  this.ready = function(){
    this.character[0]["x_position"] = 0;
    this.character[0]["y_position"] = 0;
    this.character[1]["x_position"] = 240-32;
    this.character[1]["y_position"] = 0;
    this.character[2]["x_position"] = 0;
    this.character[2]["y_position"] = 480-32;
    this.character[3]["x_position"] = 240-32;
    this.character[3]["y_position"] = 480-32;
    this.boss["x_position"] = 120;
    this.boss["y_position"] = 240;

      if(this.userNumber == 1){
          this.character[0]["class"] = "healer";
          this.character[0]["hp"] = 3000;
          this.character[0]["hp_full"] = 3000;
          this.boss["hp"] = 5000;
          this.boss["hp_full"] = 5000;
      }else if(this.userNumber == 2){
          this.character[0]["class"] = "healer";
          this.character[0]["hp"] = 3000;
          this.character[0]["hp_full"] = 3000;
          this.character[1]["class"] = "warrior";
          this.character[1]["hp"] = 6000;
          this.character[1]["hp_full"] = 6000;
          this.boss["hp"] = 12000;
          this.boss["hp_full"] = 12000;
      }else if(this.userNumber == 3){
          this.character[0]["class"] = "dealer";
          this.character[0]["hp"] = 2000;
          this.character[0]["hp_full"] = 2000;
          this.character[1]["class"] = "healer";
          this.character[1]["hp"] = 3000;
          this.character[1]["hp_full"] = 3000;
          this.character[2]["class"] = "warrior";
          this.character[2]["hp"] = 6000;
          this.character[2]["hp_full"] = 6000;
          this.boss["hp"] = 20000;
          this.boss["hp_full"] = 20000;
      }else if(this.userNumber == 4){
          this.character[0]["class"] = "dealer";
          this.character[0]["hp"] = 2000;
          this.character[0]["hp_full"] = 2000;
          this.character[1]["class"] = "dealer";
          this.character[1]["hp"] = 2000;
          this.character[1]["hp_full"] = 2000;
          this.character[2]["class"] = "healer";
          this.character[2]["hp"] = 3000;
          this.character[2]["hp_full"] = 3000;
          this.character[3]["class"] = "warrior";
          this.character[3]["hp"] = 6000;
          this.character[3]["hp_full"] = 6000;
          this.boss["hp"] = 28000;
          this.boss["hp_full"] = 28000;
      }
      io.to("Room"+this.roomNumber).emit('action'+this.roomNumber,{"character1_exist" : this.character[0]["exist"] ,"character1_x" : this.character[0]["x_position"], "character1_y" : this.character[0]["y_position"],
      "character2_exist" : this.character[1]["exist"] ,"character2_x" : this.character[1]["x_position"], "character2_y" : this.character[1]["y_position"],
      "character3_exist" : this.character[2]["exist"] ,"character3_x" : this.character[2]["x_position"], "character3_y" : this.character[2]["y_position"],
      "character4_exist" : this.character[3]["exist"] ,"character4_x" : this.character[3]["x_position"], "character4_y" : this.character[3]["y_position"],
      "boss_x" : this.boss["x_position"], "boss_y" : this.boss["y_position"], "boss_hp" : this.boss["hp"], "boss_hp_full" : this.boss["y_position"],
      "player1_class" : this.character[0]["class"], "player2_class" : this.character[1]["class"], "player3_class" : this.character[2]["class"], "player4_class" : this.character[3]["class"],
      "player1_hp_full" : this.character[0]["hp_full"], "player2_hp_full" : this.character[1]["hp_full"], "player3_hp_full" : this.character[2]["hp_full"], "player4_hp_full" : this.character[3]["hp_full"],
      "player1_hp" : this.character[0]["hp"], "player2_hp" : this.character[1]["hp"], "player3_hp" : this.character[2]["hp"], "player4_hp" : this.character[3]["hp"],
      });
  }
  var playAlert;

  this.start = function(){

    console.log("GAME START in " + this.roomNumber + " room!");
    var oInstance = this;

    this.playAlert = setInterval(function(){
      // 1프레임당 행동을 모두 계산해서 전부 emit 한다.
      // 1. 플레이어의 이동 처리
      for(var i = 0; i<oInstance.userNumber; i++){
        oInstance.character[i]["x_position"] += oInstance.character[i]["x_speed"];
        if(oInstance.character[i]["x_position"] < 0){
          oInstance.character[i]["x_position"] = 0;
        }
        if(oInstance.character[i]["x_position"] > 240){
          oInstance.character[i]["x_position"] = 240;
        }
        oInstance.character[i]["y_position"] += oInstance.character[i]["y_speed"];
        if(oInstance.character[i]["y_position"] < 0){
          oInstance.character[i]["y_position"] = 0;
        }
        if(oInstance.character[i]["y_position"] > 480){
          oInstance.character[i]["y_position"] = 480;
        }
      }

      // 2. 플레이어의 공격 처리

      for(var i = 0; i<oInstance.userNumber; i++){
        if(oInstance.character[i]["attack_usage"] == 1){
          oInstance.character[i]["attack_usage"] = 0;
          oInstance.character[i]["attack_cooldown"] = 63;
          oInstance.p_attack[i] = 31;
          // 공격했다면 피격했는지 확인 후 피격당했다면 보스 체력을 깎는다.
          var distance_x = (oInstance.character[i]["x_position"] - oInstance.boss["x_position"]) * (oInstance.character[i]["x_position"] - oInstance.boss["x_position"]);
          var distance_y = (oInstance.character[i]["y_position"] - oInstance.boss["y_position"]) * (oInstance.character[i]["y_position"] - oInstance.boss["y_position"]);
          if(oInstance.character[i]["class"] == "warrior" || oInstance.character[i]["class"] == "healer"){
            // 공격시 일정 범위에 있다면 보스의 체력을 깎고 깎은 사람의 aggrosive를 늘린다.
            if(distance_x + distance_y < 4000){
              oInstance.p_hit[i] = 31;
              oInstance.boss["hp"]-=200;
              if(oInstance.character[i]["class"] == "warrior"){
                oInstance.character[i]["aggrosive"] += 400;
              }else{
                oInstance.character[i]["aggrosive"] += 200;
              }
            }
          }else{
            if(distance_x + distance_y < 6000){
              oInstance.p_hit[i] = 1;
              oInstance.boss["hp"]-=400;
              oInstance.character[i]["aggrosive"] += 400;
            }
          }
        }
        oInstance.character[i]["attack_cooldown"] -= 1;
        if(oInstance.character[i]["attack_cooldown"] < 0){
          oInstance.character[i]["attack_cooldown"] = 0;
        }
      }
      for(var i = 0 ; i < oInstance.userNumber; i++){

        oInstance.p_hit[i] -= 1;
        oInstance.p_attack[i] -= 1;
        if(oInstance.p_hit[i] < 0){
          oInstance.p_hit[i] = 0;
        }
        if(oInstance.p_attack[i] < 0){
          oInstance.p_attack[i] = 0;
        }
      }

      // 3. 플레이어의 스킬 처리


      for(var i = 0; i<oInstance.userNumber; i++){
        if(oInstance.character[i]["skill_usage"] == 1){
          oInstance.character[i]["skill_usage"] = 0;
          oInstance.character[i]["skill_cooldown"] = 625;
          oInstance.p_skill[i] = 31;

          switch(oInstance.character[i]["class"]){
              case "warrior" :
                  oInstance.character[i]["aggrosive"] += 1000;
                  break;
              case "dealer" :
                  var distance_x = (oInstance.character[i]["x_position"] - oInstance.boss["x_position"]) * (oInstance.character[i]["x_position"] - oInstance.boss["x_position"]);
                  var distance_y = (oInstance.character[i]["y_position"] - oInstance.boss["y_position"]) * (oInstance.character[i]["y_position"] - oInstance.boss["y_position"]);
                  if(distance_x + distance_y < 4000){
                    oInstance.p_skill_hit[i] = 31;
                    oInstance.boss["hp"]-=1000;
                    oInstance.character[i]["aggrosive"] += 1000;
                  }
                  break;
              case "healer" :
                  for(var j = 0; j < oInstance.userNumber; j++){
                    var distance_x = (oInstance.character[i]["x_position"] - oInstance.character[j]["x_position"]) * (oInstance.character[i]["x_position"] - oInstance.character[j]["x_position"]);
                    var distance_y = (oInstance.character[i]["y_position"] - oInstance.character[j]["y_position"]) * (oInstance.character[i]["y_position"] - oInstance.character[j]["y_position"]);
                    if(distance_x + distance_y < 4000){
                      oInstance.character[j]["hp"] += 500;
                      oInstance.character[i]["aggrosive"] += 500;
                    }
                  }
                  break;
          }
        }
        oInstance.character[i]["skill_cooldown"] -= 1;
        if(oInstance.character[i]["skill_cooldown"] < 0){
          oInstance.character[i]["skill_cooldown"] = 0;
        }

      }

      for(var i = 0; i<oInstance.userNumber; i++){
        oInstance.p_skill_hit[i] -=1;
        oInstance.p_skill[i] -=1;
        if(oInstance.p_skill_hit[i] < 0){
          oInstance.p_skill_hit[i] = 0;
        }
        if(oInstance.p_skill[i] < 0){
          oInstance.p_skill[i] = 0;
        }
      }

      // 4. 보스의 이동 처리

      // 가장 어그로 수치가 높은 캐릭터를 가지고 온다.
      if(oInstance.boss["dontmove"] == 0){
        var most_aggrosive_person = 0;
        var most_aggrosive = -100;
        for(var i = 0; i < oInstance.userNumber ; i++){
            if( oInstance.character[i]["aggrosive"] > most_aggrosive ){
                most_aggrosive_person = i;
                most_aggrosive = oInstance.character[i]["aggrosive"];
            }
        }
        oInstance.boss["now_aggrouser"] = most_aggrosive_person;

        var distance_x = (oInstance.boss["x_position"] - oInstance.character[most_aggrosive_person]["x_position"]) * (oInstance.boss["x_position"] - oInstance.character[most_aggrosive_person]["x_position"]);
        var distance_y = (oInstance.boss["y_position"] - oInstance.character[most_aggrosive_person]["y_position"]) * (oInstance.boss["y_position"] - oInstance.character[most_aggrosive_person]["y_position"]);
        var distance = distance_x + distance_y;
        var destination_x = 1;
        var destination_y = 1;
        if(oInstance.boss["x_position"] > oInstance.character[most_aggrosive_person]["x_position"]){
          destination_x = -1;
        }
        if(oInstance.boss["y_position"] > oInstance.character[most_aggrosive_person]["y_position"]){
          destination_y = -1;
        }

        if(distance == 0){
            oInstance.boss["x_speed"] = 0;
            oInstance.boss["y_speed"] = 0;
        }else{
            var sinTheta = distance_x/distance;
            var cosinTheta = distance_y/distance;
            oInstance.boss["x_speed"] = 0.45 * destination_x * sinTheta * 1;
            oInstance.boss["y_speed"] = 0.45 * destination_y * cosinTheta * 1;
        }

        // 어그로수치가 높은 방향을 바라본다.
        oInstance.boss["x_position"] += oInstance.boss["x_speed"];
        oInstance.boss["y_position"] += oInstance.boss["y_speed"];
        if(oInstance.boss["x_position"] < 30){
          oInstance.boss["x_position"] = 30;
        }
        if(oInstance.boss["x_position"] > 210){
          oInstance.boss["x_position"] = 210;
        }
        if(oInstance.boss["y_position"] < 50){
          oInstance.boss["y_position"] = 50;
        }
        if(oInstance.boss["y_position"] > 430){
          oInstance.boss["y_position"] = 430;
        }
      }


      // 5. 보스의 일반 패턴 생성 관련 처리
      oInstance.bossNormalAttackFactory();


      // 6. 보스의 일반 패턴 진행 관련 처리
      oInstance.boss["attack_cooldown"] -= 1;
      var p_hit_range = new Array(0,0,0,0);
      if(oInstance.boss["attack_usage"] == 1){

        if(oInstance.boss["attack_ready"] >= oInstance.boss["attack_max_charge"]){
          // 데미지 처리 구현을 한다.
          for(var i = 0; i<oInstance.userNumber; i++){

            var distance_x = (oInstance.character[i]["x_position"] - oInstance.boss["attack_x"]) * (oInstance.character[i]["x_position"] - oInstance.boss["attack_x"]);
            var distance_y = (oInstance.character[i]["y_position"] - oInstance.boss["attack_y"]) * (oInstance.character[i]["y_position"] - oInstance.boss["attack_y"]);
            if(distance_x + distance_y < oInstance.boss["attack_radius"] * oInstance.boss["attack_radius"]  ){
                switch(oInstance.boss["attack_kind"]){
                  case 'A':
                    oInstance.character[i]["hp"] -= 300;
                    break;
                  case 'B':
                    oInstance.character[i]["hp"] -= 150;
                    break;
                  case 'C':
                    oInstance.character[i]["hp"] -= 200;
                    break;
                }
              }
            }
            oInstance.boss["dontmove"] = 0;
            oInstance.boss["attack_usage"] = 0;
            oInstance.boss["attack_max_charge"] = 0;
            oInstance.boss["attack_ready"] = 0;
        }else{
          oInstance.boss["attack_ready"] += 1;
        }
        if(oInstance.boss["attack_usage"] == 1){
          for(var i = 0; i<oInstance.userNumber; i++){

            var distance_x = (oInstance.character[i]["x_position"] - oInstance.boss["attack_x"]) * (oInstance.character[i]["x_position"] - oInstance.boss["attack_x"]);
            var distance_y = (oInstance.character[i]["y_position"] - oInstance.boss["attack_y"]) * (oInstance.character[i]["y_position"] - oInstance.boss["attack_y"]);
            if(distance_x + distance_y < oInstance.boss["attack_radius"] * oInstance.boss["attack_radius"]  ){
                p_hit_range[i] = 1;
              }
            }
        }

      }

      for(var i=0; i<oInstance.bossMissile.length; i++){
          // 총알의 이동 처리.
          oInstance.bossMissile[i]["x_position"] += oInstance.bossMissile[i]["x_speed"];
          oInstance.bossMissile[i]["y_position"] += oInstance.bossMissile[i]["y_speed"];
      }

      for(var i=0; i<oInstance.bossMissile.length; i++){
          // 총알의 피격 및 배열 제거 처리.
          for(var j = 0 ; j < oInstance.userNumber ; j++ ){
            try {
              if(oInstance.bossMissile[i] == null){
                continue;
              }
              var distance_x = (oInstance.bossMissile[i]["x_position"] - oInstance.character[j]["x_position"] ) * (oInstance.bossMissile[i]["x_position"] - oInstance.character[j]["x_position"] );
              var distance_y = (oInstance.bossMissile[i]["y_position"] - oInstance.character[j]["y_position"] ) * (oInstance.bossMissile[i]["y_position"] - oInstance.character[j]["y_position"] );
              if(distance_x + distance_y < oInstance.boss["attack_radius"] * oInstance.boss["attack_radius"] ){
                oInstance.character[j]["hp"] -= 150;
                oInstance.bossMissile.splice(i, 1);
                continue;
              }else if(oInstance.bossMissile[i]["x_position"] < -30){
                oInstance.bossMissile.splice(i, 1);
                continue;
              }
              else if(oInstance.bossMissile[i]["x_position"] > 270){
                oInstance.bossMissile.splice(i, 1);
                continue;
              }
              else if(oInstance.bossMissile[i]["y_position"] < -60){
                oInstance.bossMissile.splice(i, 1);
                continue;
              }
              else if(oInstance.bossMissile[i]["y_position"] > 540){
                oInstance.bossMissile.splice(i, 1);
                continue;
              }
            }
            catch(error) {
                console.log(error);
                continue;
            }
          }
      }
      // 8. 보스의 특수 패턴 생성 관련 처리
      oInstance.bossPatternAttackFactory();

      // 9. 보스의 특수 패턴 진행 관련 처리
      oInstance.bossPatternAttackExecute();

      // 10. 유저가 죽었는지 살았는지 체크 후 죽었으면 die 에 표시...
      for(var i = 0;i<oInstance.userNumber ; i++){
        if((oInstance.character[i]["die"] == 0)&&(oInstance.character[i]["hp"] <= 0)){
          console.log( (i+1) + "User Die!");
          oInstance.character[i]["die"] = 1;
          oInstance.character[i]["x_speed"] = 0;
          oInstance.character[i]["y_speed"] = 0;
          oInstance.character[i]["x_position"] = -500;
          oInstance.character[i]["y_position"] = -500;
          oInstance.character[i]["aggrosive"] = -1;
        }
      }
        var dieUser = 0;
        for(var i = 0; i <oInstance.userNumber ; i++){
            if(oInstance.character[i]["die"] == 1){
                dieUser+=1;
            }
        }
        if(dieUser == oInstance.userNumber){
          console.log("all player die");
          clearInterval(oInstance.playAlert);
          io.to('Room'+oInstance.roomNumber).emit("lose");
          executeGame[oInstance.roomNumber-1] = false;
        }
        for(var i = 0; i<oInstance.userNumber; i++){
          if(oInstance.character[i]["die"] == 1){
            oInstance.character[i]["x_position"] = -500;
            oInstance.character[i]["y_position"] = -500;
          }
        }

      //11 보스가 죽었는지 살았는지 체크 후 죽었으면 승리 발송
      if(oInstance.boss["hp"] <= 0){
        console.log("boss die");
        clearInterval(oInstance.playAlert);
        io.to('Room'+oInstance.roomNumber).emit("win");
        executeGame[oInstance.roomNumber-1] = false;
      }

        io.to('Room'+oInstance.roomNumber).emit('action'+oInstance.roomNumber,{
        "character1_exist" : oInstance.character[0]["exist"] ,"character1_x" : oInstance.character[0]["x_position"], "character1_y" : oInstance.character[0]["y_position"],
        "character2_exist" : oInstance.character[1]["exist"] ,"character2_x" : oInstance.character[1]["x_position"], "character2_y" : oInstance.character[1]["y_position"],
        "character3_exist" : oInstance.character[2]["exist"] ,"character3_x" : oInstance.character[2]["x_position"], "character3_y" : oInstance.character[2]["y_position"],
        "character4_exist" : oInstance.character[3]["exist"] ,"character4_x" : oInstance.character[3]["x_position"], "character4_y" : oInstance.character[3]["y_position"],
        "boss_x" : oInstance.boss["x_position"], "boss_y" : oInstance.boss["y_position"], "boss_hp" : oInstance.boss["hp"], "boss_hp_full" : oInstance.boss["hp_full"],
        "player1_attack" : oInstance.p_attack[0], "player2_attack" : oInstance.p_attack[1], "player3_attack" : oInstance.p_attack[2], "player4_attack" : oInstance.p_attack[3],
        "player1_skill" : oInstance.p_skill[0], "player2_skill" : oInstance.p_skill[1], "player3_skill" : oInstance.p_skill[2], "player4_skill" : oInstance.p_skill[3],
        "player1_skill_hit" : oInstance.p_skill_hit[0], "player2_skill_hit" : oInstance.p_skill_hit[1], "player3_skill_hit" : oInstance.p_skill_hit[2], "player4_skill_hit" : oInstance.p_skill_hit[3],
        "player1_hitboss" : oInstance.p_hit[0], "player2_hitboss" : oInstance.p_hit[1], "player3_hitboss" : oInstance.p_hit[2], "player4_hitboss" : oInstance.p_hit[3],
        "player1_class" : oInstance.character[0]["class"], "player2_class" : oInstance.character[1]["class"], "player3_class" : oInstance.character[2]["class"], "player4_class" : oInstance.character[3]["class"],
        "player1_hp_full" : oInstance.character[0]["hp_full"], "player2_hp_full" : oInstance.character[1]["hp_full"], "player3_hp_full" : oInstance.character[2]["hp_full"], "player4_hp_full" : oInstance.character[3]["hp_full"],
        "player1_hp" : oInstance.character[0]["hp"], "player2_hp" : oInstance.character[1]["hp"], "player3_hp" : oInstance.character[2]["hp"], "player4_hp" : oInstance.character[3]["hp"],
        "boss_missile" : oInstance.bossMissile, "boss_attack_x" : oInstance.boss["attack_x"], "boss_attack_y" : oInstance.boss["attack_y"], "boss_attack_radius" : oInstance.boss["attack_radius"],
        "boss_attack_ready" : oInstance.boss["attack_ready"], "boss_attack_max" : oInstance.boss["attack_max_charge"], "boss_attack_usage" : oInstance.boss["attack_usage"], "boss_attack_type" : oInstance.boss["attack_kind"],
        "player1_die" : oInstance.character[0]["die"], "player2_die" : oInstance.character[1]["die"], "player3_die" : oInstance.character[2]["die"], "player4_die" : oInstance.character[3]["die"],
        "player1_hit_range" : p_hit_range[0], "player2_hit_range" : p_hit_range[1], "player3_hit_range" : p_hit_range[2], "player4_hit_range" : p_hit_range[3],
        "boss_pattern_usage" : oInstance.boss["pattern_usage"], "boss_pkind" : oInstance.boss["pattern_kind"],
        "boss_pattern_x1" : oInstance.boss["pattern_x1"], "boss_pattern_y1" : oInstance.boss["pattern_y1"], "boss_pattern_radius1" : oInstance.boss["pattern_radius1"],
        "boss_pattern_x2" : oInstance.boss["pattern_x2"], "boss_pattern_y2" : oInstance.boss["pattern_y2"], "boss_pattern_radius2" : oInstance.boss["pattern_radius2"],
        "boss_pattern_x3" : oInstance.boss["pattern_x3"], "boss_pattern_y3" : oInstance.boss["pattern_y3"], "boss_pattern_radius3" : oInstance.boss["pattern_radius3"],
        "boss_pattern_x4" : oInstance.boss["pattern_x4"], "boss_pattern_y4" : oInstance.boss["pattern_y4"], "boss_pattern_radius4" : oInstance.boss["pattern_radius4"],
        "boss_pattern_max_time1" : oInstance.boss["pattern_max_time1"], "boss_pattern_max_time2" : oInstance.boss["pattern_max_time2"],
        "boss_pattern_max_time3" : oInstance.boss["pattern_max_time3"], "boss_pattern_max_time4" : oInstance.boss["pattern_max_time4"],
        "boss_pattern_time1" : oInstance.boss["pattern_time1"], "boss_pattern_time2" : oInstance.boss["pattern_time2"],
        "boss_pattern_time3" : oInstance.boss["pattern_time3"], "boss_pattern_time4" : oInstance.boss["pattern_time4"],
        "boss_pattern_success1" : oInstance.boss["pattern_success1"], "boss_pattern_success2" : oInstance.boss["pattern_success2"],
        "boss_pattern_success3" : oInstance.boss["pattern_success3"], "boss_pattern_success4" : oInstance.boss["pattern_success4"],
        });
        for(var i = 0; i < oInstance.userNumber; i++){
          try{
              io.to(room[oInstance.roomNumber-1]["UserArray"][i].id).emit('cooldown'+oInstance.roomNumber,{
                "player_cooldown" : oInstance.character[i]["skill_cooldown"], "player_cooldown_max" : 625,
              });
          }catch(error){
              console.log(error);
          }

        }

    },16);
  }

}



io.on('connection', function(socket){ //3
      console.log('user connected: ', socket.id);
      socket.on('postnicknameimage',function(object){
        var nickname = object["nickname"];
        var url = object["url"];
        nowConnectUser.push({"id" : socket.id , "nickname" : nickname, "url" : url});
      });
      socket.on('disconnect', function(){ //3-2
        console.log('user disconnected: ', socket.id);
        for(var i =0 ;i<nowConnectUser.length;i++){
           if(nowConnectUser[i].id == socket.id){
               nowConnectUser.splice(i, 1);
               break;
           }
        }
        for(var j =0; j< room.length; j++){
          for(var i =0 ;i<room[j]["UserArray"].length;i++){
             if(room[j]["UserArray"][i].id == socket.id){
                 room[j]["UserArray"].splice(i, 1);
                 break;
             }
          }
        }
      });
      socket.on('enterroom', function(object){ // Room
        var roomNumber = object["room"];
        socket.join("Room"+roomNumber);
        for(var i = 0; i<nowConnectUser.length; i++){
          if(nowConnectUser[i]["id"] == socket.id){
            room[roomNumber-1]["UserArray"].push({"id" : socket.id , "nickname" : nowConnectUser[i]["nickname"], "url" : nowConnectUser[i]["url"]  });
            break;
          }
        }
        var data = {};
        for(var i = 0 ; i< room[roomNumber-1]["UserArray"].length; i++){
          data[i] = {"nickname" : room[roomNumber-1]["UserArray"][i]["nickname"], "url" :room[roomNumber-1]["UserArray"][i]["url"] };
        }
        setTimeout(function(){
          for(var i = 0; i < room[roomNumber-1]["UserArray"].length ; i++){
            io.to(room[roomNumber-1]["UserArray"][i]["id"]).emit('givetoroomnickname',data);
          }
        },800);
      });
      socket.on('exitroom',function(object){
        var roomNumber = object["room"];
        socket.leave("Room"+roomNumber)
        for(var i =0 ;i<room[roomNumber-1]["UserArray"].length;i++){
           if(room[roomNumber-1]["UserArray"][i].id == socket.id){
               room[roomNumber-1]["UserArray"].splice(i, 1);
               break;
           }
        }
        io.to('Room'+roomNumber ).emit("lose");
        clearInterval(gameState[roomNumber-1].playAlert);
        executeGame[roomNumber-1] = false;
        var data = {};
        for(var i = 0 ; i< room[roomNumber-1]["UserArray"].length; i++){
          data[i] = {"nickname" : room[roomNumber-1]["UserArray"][i]["nickname"], "url" :room[roomNumber-1]["UserArray"][i]["url"] };
        }
        for(var i = 0; i < room[roomNumber-1]["UserArray"].length ; i++){
          io.to(room[roomNumber-1]["UserArray"][i]["id"]).emit('givetoroomnickname',data);
        }
      });

      socket.on('refreshmain',function(){
        io.to(socket.id).emit('waitingroomuser',{"room1" : room[0]["UserArray"].length, "room2" : room[1]["UserArray"].length, "room3" : room[2]["UserArray"].length, "room4" : room[3]["UserArray"].length });
      });

      socket.on('gamestart',function(object){
          var roomNumber = object["room"];
          if(executeGame[roomNumber-1] == false){
            executeGame[roomNumber-1] = true;
            io.to("Room"+roomNumber).emit('gogameserver');
            gameState[roomNumber-1] = new game(roomNumber,room[roomNumber-1]["UserArray"].length);
            gameState[roomNumber-1].init();
            for(var i = 0; i< room[roomNumber-1]["UserArray"].length ;i++){
                gameState[roomNumber-1].character[i]["id"] = room[roomNumber-1]["UserArray"][i]["id"];
                gameState[roomNumber-1].character[i]["exist"] = 1;
            }
            setTimeout(function(){
            gameState[roomNumber-1].ready();
            for(var i = 0; i<room[roomNumber-1]["UserArray"].length; i++){
                io.to(room[roomNumber-1]["UserArray"][i]["id"]).emit('yourClass',gameState[roomNumber-1].character[i]["class"]);
              }
            },500);
            setTimeout(function(){
              gameState[roomNumber-1].start();
            },2000);
          }
      });

      for(var r = 0; r < room.length; r++){
        var rInstance = this;
        socket.on('click_position' + (r+1) , function(object){ //3-2
          var roomNumber = object["roomnumber"]-1;
          for(var i = 0; i<room[roomNumber]["UserArray"].length; i++){

            if((gameState[roomNumber].character[i]["die"]==0) && (gameState[roomNumber].character[i]["id"]==socket.id)){
              if(object["action"] == 0){
                gameState[roomNumber].character[i]["x_destination"] = object["x_position"];
                gameState[roomNumber].character[i]["y_destination"] = object["y_position"];
                // 사인과 코사인값을 구하자.
                var distance_x = (gameState[roomNumber].character[i]["x_destination"] - gameState[roomNumber].character[i]["x_position"]) * (gameState[roomNumber].character[i]["x_destination"] - gameState[roomNumber].character[i]["x_position"]);
                var distance_y = (gameState[roomNumber].character[i]["y_destination"] - gameState[roomNumber].character[i]["y_position"]) * (gameState[roomNumber].character[i]["y_destination"] - gameState[roomNumber].character[i]["y_position"]);
                var distance = distance_x + distance_y;
                var destination_x = 1;
                var destination_y = 1;
                if(gameState[roomNumber].character[i]["x_destination"] < gameState[roomNumber].character[i]["x_position"]){
                  destination_x = -1;
                }
                if(gameState[roomNumber].character[i]["y_destination"] < gameState[roomNumber].character[i]["y_position"]){
                  destination_y = -1;
                }
                if(distance == 0){
                    gameState[roomNumber].character[i]["x_speed"] = 0;
                    gameState[roomNumber].character[i]["y_speed"] = 0;
                }else{
                    var sinTheta = distance_x/distance;
                    var cosinTheta = distance_y/distance;
                    gameState[roomNumber].character[i]["x_speed"] = destination_x * sinTheta * 1;
                    gameState[roomNumber].character[i]["y_speed"] = destination_y * cosinTheta * 1;
                }
              }else if(object["action"] == 1){
                    if(gameState[roomNumber].character[i]["attack_usage"] == 0 && gameState[roomNumber].character[i]["attack_cooldown"] <= 0){
                        gameState[roomNumber].character[i]["attack_usage"] = 1;
                    }
              }else if(object["action"] == 2){
                    if(gameState[roomNumber].character[i]["skill_usage"] == 0 && gameState[roomNumber].character[i]["skill_cooldown"] <= 0){
                        gameState[roomNumber].character[i]["skill_usage"] = 1;
                    }
              }
              break;
            }
          }
        });
      }

});


/*
setInterval(function() {
  console.log("All User :");
  console.log(nowConnectUser);
  for(var i = 0; i<room.length; i++){
  console.log("room" + (i+1) + " User");
  console.log(room[i]);
}
}, 5000);
*/
http.listen(80, function(){ //4
  console.log('Port 80 Waiting Room Server On!');
});
